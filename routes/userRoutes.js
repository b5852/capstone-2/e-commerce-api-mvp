const express = require("express");

// Allows access to Http method middlewares that makes it easier to create routes for our applcaition
const router = express.Router();

// The "userControllers" allow us to use the function defined inside it
const userControllers = require("../controllers/userControllers");

const auth = require("../auth");


// Router for the user registration
router.post("/signup", (req, res) =>{
	if(req.body.email == "" && req.body.password == "" && req.body.completeName == ""){

		res.send("Please input Name, Email, and Password!")

	}
	else if(req.body.email === "" || req.body.email === undefined) {

		res.send("Please input your email!")

	}
	else if(req.body.completeName === "" || req.body.completeName === undefined) {

		res.send("Please input your complete name!")

	}
	else if(req.body.password === "" || req.body.password === undefined){

		res.send("Please input your Password!")

	}
	else {
		userControllers.userSignup(req.body).then(resultFromController => res.send(resultFromController));
	}
})

// Route for the user login (with token creation)
router.post("/login", (req, res) =>{

	if(req.body.email == "" && req.body.password == ""){

		res.send("Please login using Email and Password!")

	}
	else if(req.body.email === "" || req.body.email === undefined) {

		res.send("Please input your email!")

	}
	else if(req.body.password === "" || req.body.password === undefined){

		res.send("Please input your Password!")

	}
	else {
		userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController))
	}
})


// Route for the user Details
router.get("/myDetails", auth.verify, (req, res) =>{

	const userData = auth.decode(req.headers.authorization); //contains the token
	
	userControllers.userData({userId: userData.id}).then(resultFromController => res.send(resultFromController))
})


//Route for retrieving all the courses
router.get("/allUsers", auth.verify, (req, res) =>{

	const userDataLogin = auth.decode(req.headers.authorization);

	// console.log(userDataLogin.isAdmin);

	if (userDataLogin.isAdmin !== true) {
		res.send("You're not allowed to access this page")
	}
	else{
		userControllers.getAlluser().then(resultFromController => res.send(resultFromController))
	}
		
})

// Route to Update user as a Admin
router.patch("/:id/setUserAdmin", auth.verify, (req, res) => {
	const userDataLogin = auth.decode(req.headers.authorization);

	if (userDataLogin.isAdmin !== true) {
		res.send("You're not allowed to access this page")
	}
	else{

		userId = req.params.id;
		if (userId.length === 24) {
			userControllers.setAdmin(req.params.id).then(resultFromController => res.send(resultFromController))
		}
		else {
			response.send("Invalid user ID");
		}
	}
	
})



module.exports = router;