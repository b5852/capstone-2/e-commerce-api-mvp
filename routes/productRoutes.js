const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

router.post("/", auth.verify, (request, response) =>{
	const userData = auth.decode(request.headers.authorization); //contains the token
	console.log(`userData Result: ${userData.isAdmin}`)
	// if isAdmin is equal to true 
	if (userData.isAdmin !== true) {
			response.send("You don’t have permission on this page!");
		}
		// isAdmin is equal to false
		else {
			if(request.body.name == "" && request.body.description == "" && request.body.price == "" && request.body.stocks == ""){

				res.send("Please input name, description, price, and stocks!")

			}
			else if(request.body.name === "" || request.body.name === undefined) {

				res.send("Please input product name!")

			}
			else if(request.body.description === "" || request.body.description === undefined) {

				res.send("Please input product description!")

			}
			else if(request.body.price === "" || request.body.price === undefined){

				res.send("Please input product price!")

			}
			else if(request.body.stocks === "" || request.body.stocks === undefined){

				res.send("Please input product stocks!")

			}
			else
			{
				productControllers.addProduct(request.body).then(resultFromController => response.send(resultFromController));
			}
		}
})

//Route for retrieving all active product
router.get("/", (request, response) =>{
	productControllers.getAllActive().then(resultFromController => response.send(resultFromController));
		
})


//Route for retrieving product orders
router.get("/productOrder", auth.verify, (request, response) =>{

	const userDataLogin = auth.decode(request.headers.authorization);

	if (userDataLogin.isAdmin !== true) {
		response.send("You're not allowed to access this page")
	}
	else{
		productControllers.getAllproductOrder().then(resultFromController => response.send(resultFromController));
	}
	
		
})


// Route for retrieving a specific product
router.get("/:productId", (request, response) =>{
	productId = request.params.productId;
	if (productId.length === 24) {
		productControllers.getProduct(productId).then(resultFromController => response.send(resultFromController));
	} 
	else {
		response.send("Invalid product ID");
	}
	
		
})


// Route for updating a product
router.put("/:productId", auth.verify, (request, response) =>{

	const userData = auth.decode(request.headers.authorization); 

	// if isAdmin is equal to true 
	if (userData.isAdmin !== true) {
			response.send("You don’t have permission on this page!");
		}
		// isAdmin is equal to false
		else {
			productId = request.params.productId;
			if (productId.length === 24) {
				productControllers.updateProduct(request.params.productId, request.body).then(resultFromController => response.send(resultFromController));
			}
			else {
				response.send("Invalid product ID");
			}
		}
})




// Route to archieve a product
router.patch("/:productId/archieve", auth.verify, (request, response) =>{
	// console.log(request.params.productId);

	const userData = auth.decode(request.headers.authorization); 

	// if isAdmin is equal to true 
	if (userData.isAdmin !== true) {
			response.send("You don’t have permission on this page!");
		}
		// isAdmin is equal to false
		else {
			productId = request.params.productId;
			if (productId.length === 24) {
				productControllers.archieveProduct(request.params.productId).then(resultFromController => response.send(resultFromController));
			}
			else {
				response.send("Invalid product ID");
			}
		}
	
})


module.exports = router;