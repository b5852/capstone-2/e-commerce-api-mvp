const express = require("express");

// Allows access to Http method middlewares that makes it easier to create routes for our applcaition
const router = express.Router();

// The "userControllers" allow us to use the function defined inside it
const orderDetailControllers = require("../controllers/orderDetailControllers");

const auth = require("../auth");


// Route to add an Order Details
router.post("/", auth.verify, (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	let data = {
		// userId will ne retrieved from the request header
		userId: userData.id,
		productId: request.body.productId,
		quantity: request.body.quantity
	}
	if (userData.isAdmin) {
		response.send("You're not allowed to access this page")
	}
	else{
		if (data.productId.length === 24 && data.userId.length === 24) {

			if(request.body.quantity === "" || request.body.quantity === undefined) {

				response.send("Please input your product quantity!")

			}
			else {

				orderDetailControllers.productOrder(data).then(resultFromController => response.send(resultFromController))

			}
			
		}
		else
		{
			response.send("Invalid product ID")
		}
		
	}
	
})


//Route for retrieving all orders
router.get("/", auth.verify, (request, response) =>{

	const userDataLogin = auth.decode(request.headers.authorization);
	// console.log(userDataLogin.isAdmin)

	if (userDataLogin.isAdmin === true) {
		response.send("You're not allowed to access this page")
	}
	else{
		orderDetailControllers.viewCart({userId: userDataLogin.id}).then(resultFromController => response.send(resultFromController));
	}

	
		
})

//Route for checkout
router.patch("/checkout", auth.verify, (request, response) =>{

	const userDataLogin = auth.decode(request.headers.authorization);
	// console.log(userDataLogin.isAdmin)


	let data = {
		// userId will ne retrieved from the request header
		userId: userDataLogin.id,
		contactNumber: request.body.contactNumber,
		shippingAddress: request.body.shippingAddress,
		modeOfPayment: request.body.modeOfPayment
	}

	if (userDataLogin.isAdmin === true) {
		response.send("You're not allowed to access this page")
	}
	else{

		if(request.body.contactNumber === "" || request.body.contactNumber === undefined) {

			response.send("Please input contact number!")

		}
		else if(request.body.shippingAddress === "" || request.body.shippingAddress === undefined) {

			response.send("Please input shipping address!")

		}
		else if(request.body.modeOfPayment === "" || request.body.modeOfPayment === undefined) {

			response.send("Please input mode of payment!")

		}
		else {
			orderDetailControllers.checkout(data).then(resultFromController => response.send(resultFromController));
		}
	}

	
		
})


//Route for retrieving all the courses
router.get("/allOrder", auth.verify, (req, res) =>{

	const userDataLogin = auth.decode(req.headers.authorization);

	// console.log(userDataLogin.isAdmin);

	if (userDataLogin.isAdmin !== true) {
		res.send("You're not allowed to access this page")
	}
	else{
		orderDetailControllers.getAllOrder(userDataLogin).then(resultFromController => res.send(resultFromController))
	}
		
})


//Route for retrieving all the courses
router.get("/completedOrder", auth.verify, (req, res) =>{

	const userDataLogin = auth.decode(req.headers.authorization);
	let data = {
		// userId will ne retrieved from the request header
		userId: userDataLogin.id,
		isCompleted: true
	}
	// console.log(userDataLogin.isAdmin);

	if (userDataLogin.isAdmin === true) {
		res.send("You're not allowed to access this page")
	}
	else{
		orderDetailControllers.getAllCompletedOrder(data).then(resultFromController => res.send(resultFromController))
	}
		
})


module.exports = router;