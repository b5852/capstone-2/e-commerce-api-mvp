const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Please input product name."]
	},
	description: {
		type: String,
		required: [true, "Please input short description about your product."]
	},
	price: {
		type: Number,
		required: [true, "Please input price amount."]
	},
	stocks: {
		type: Number,
		required: [true, "Please input number of quantity available."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});


module.exports = mongoose.model("Product", courseSchema);
