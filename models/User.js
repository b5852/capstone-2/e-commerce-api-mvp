const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	completeName: {
		type: String,
		required: [true, "Please input your complete name"]
	},
	email: {
		type: String,
		required: [true, "Email is required! Please input your email"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});


module.exports = mongoose.model("User", userSchema);
