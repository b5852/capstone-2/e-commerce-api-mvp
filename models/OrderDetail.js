const mongoose = require("mongoose");

const orderDetailsSchema = new mongoose.Schema({
	orderId: {
		type: String,
		required: [true, "Your order ID is not found."]
	},
	productId: {
		type: String,
		required: [true, "Product ID is not found."]
	},
	productName: {
		type: String,
		required: [true, "Product name is not found."]
	},
	price: {
		type: Number,
		required: [true, "Product price is not found."]
	},
	quantity: {
		type: Number,
		required: [true, "Product quantity is not found."]
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});


module.exports = mongoose.model("OrderDetails", orderDetailsSchema);
