const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "Your user ID is not found."]
	},
	contactNumber: {
		type: String,
		default: null
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	shippingAddress: {
		type: String,
		default: null
	},
	modeOfPayment: {
		type: String,
		default: null
	},
	isCompleted: {
		type: Boolean,
		default: false
	},
	orderOn: {
		type: Date,
		default: new Date()
	}
});


module.exports = mongoose.model("Order", orderSchema);
