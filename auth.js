
// npm install jsonwebtoken
const jwt = require("jsonwebtoken");

const securityKey = "myCapstone2ProjectZuitt";

module.exports.createAccessToken = (user) => {
	// When the user logs in, a token will be created with the user's information
	// This will be used for the token payload
	const userDataLogin = {
		id: user._id,
		completeName: user.completeName,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(userDataLogin, securityKey, {});
}

// Middleware function
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		
		token = token.slice(7, token.length)
		
		return jwt.verify(token, securityKey, (err, data) =>{
			// if jwt is not valid
			if (err) {
				console.log(`Your token is not valid! Please login again.`);
				return res.send({auth: "Token Failed"});
			} 
			else{
				// the verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function.
				next();
			}
		})
	}
	else {
		console.log(`Your token is undefined! Please login again.`);
		return res.send({auth: "Failed"});
	}
}

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
			token = token.slice(7, token.length);
			return jwt.verify(token, securityKey, (err, data)=>{
				// If JWT is not valid
				if(err){
					return null;
				}
				else{
					// Syntax: jwt.decode(token, [options])
					return jwt.decode(token, {complete: true}).payload;
				}
			})
		}
		else{
			console.log(`Your token is undefined! Please login again.`);
			return res.send({auth: "Failed"});
		}
}