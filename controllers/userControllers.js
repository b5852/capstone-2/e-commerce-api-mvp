const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");




// Router for the user registration (with email duplicate filter)
module.exports.userSignup = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if (result.length > 0) {
			console.log(`Sorry! the ${reqBody.email} is already registered to the database.\n`);
			// return false;
			return `Sorry! the ${reqBody.email} is already registered to the database.`;
		}
		// No Duplicate email found
		else {

			let newUser = new User({
				completeName: reqBody.completeName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((user, error) =>{
				if(error){
					console.log(`System error: ${error}\n`);
					// return false;
					return `System error: ${error}`;
				}
				else {
					console.log(`\nHi ${newUser.completeName}, welcome to the E-Commerce App. Below is your access token:\n${auth.createAccessToken(user)}\n`)
					// return true;
					return `Hi ${newUser.completeName}, welcome to the E-Commerce App. Below is your access token:\n${auth.createAccessToken(user)}`;
				}

			})
		}
	})


}




// Controller for the user login (with token creation)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		
		// User does not exists
		if (result == null) {
			// return false; //or
			console.log(`Sorry! your email does not exist in our database! Please try again.\n`)
			// return false;
			return `Sorry! your email does not exist in our database! Please try again.\n`;
		}
		// User exists
		else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the passwords match the result.
			if (isPasswordCorrect) {
				console.log(`Hi ${result.completeName}, welcome back to the E-Commerce App.\nAccess Token:\n${auth.createAccessToken(result)}\n`)
				// return true;
				return `Hi ${result.completeName}, welcome back to the E-Commerce App.\nAccess Token:\n${auth.createAccessToken(result)}\n`;

			}
			// Password did not match
			else {
				// return false; //or
				console.log(`Password did not match!\n`)
				// return false;
				return `Password did not match!`;
			}

		}
	})
}




// Controller for the user details (myDetails)
module.exports.userData = (dataUserLogin) => {
	return User.findById(dataUserLogin.userId).then(result =>{
		// return true;
		return `Complete Name: ${result.completeName}\nEmail: ${result.email}\n`;
	})
}

// Controller for the all user
module.exports.getAlluser = (dataUserLogin) => {
	return User.find({}).then(result => result)
}




// Controller for the set user as admin
module.exports.setAdmin = (id) =>{

	let setAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(id, setAdmin).then((result, error) =>{

		if(error){

			console.log(`System error: ${error}\n`);
			// return false;
			return `System error: ${error}`;

		}
		else{

			console.log(`User successfully add as admin!\n`)
			// return true;
			return `User successfully add as admin!`;

		}

	})

}
