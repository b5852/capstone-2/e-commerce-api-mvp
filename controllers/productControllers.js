// const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");


module.exports.addProduct = (reqBody) =>{
	
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	})

	return newProduct.save().then((product, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => result);
}

//Route for the specific product
module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}



module.exports.updateProduct = (productId, reqBody) =>{
	// Specify the fields/properties to be updated

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((updatedProduct, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}


// Archieve a product
module.exports.archieveProduct = (productId, reqBody) =>{
	// Specify the fields/properties to be updated

	let archievedProduct = {
		isActive: false

	}

	return Product.findByIdAndUpdate(productId, archievedProduct).then((productArchive, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}



// Controller for the all product order
module.exports.getAllproductOrder = () => {
	return Product.find({}).then(result => result)
}