const OrderDetail = require("../models/OrderDetail");
const Order = require("../models/Order");
const Product = require("../models/Product");
const auth = require("../auth");


module.exports.productOrder = async(data) =>{

	let isProductDetails = await Product.findById(data.productId).then((productResultData, error) => {

		if (productResultData !== null) 
		{

			if(error){
				// return false;
				return `System error: ${error}`;
			}
			else {
				// return true;
				if (productResultData.stocks >= data.quantity) {

					let stocksAvailable = {
						updateStocks: productResultData.stocks - (data.quantity)
					}
					return Order.findByIdAndUpdate(data.productId, stocksAvailable).then((updated, error) =>{
						if(error){
							return false;
						}
						else {
							let data = {
								stocks: productResultData.stocks,
								name: productResultData.name,
								price: productResultData.price,
								isExcute: true 
							}
							return data;
						}
					})
				}
				else {
					let data = {
						stocks: productResultData.stocks,
						name: productResultData.name,
						price: productResultData.price,
						isExcute: false 
					}
					return data;
				}

				
			}
		}
		else {

			let data = {
				stocks: productResultData,
				isExcute: false 
			}

			return data;
		}

	})
	if (isProductDetails.isExcute !== false) {

		// Generate Order ID

	
		let orderIdPushed = await Order.findOne({userId: data.userId, isCompleted: false}).then((orderResultData, error) => {
			
			if (error) {
				return false;
			}
			else{

				if (orderResultData === null) {

					let newOrder = new Order({
						userId: data.userId,
						isActive: false
					})

					return newOrder.save().then((myOrder, error) =>{
						if(error){
							// return false;
							return `System error: ${error}`;
						}
						else {
							let order = {
								id: myOrder.id,
								isOrder: true
							}
							return order;
						}

					})
				}
				else {
					let order = {
						id: orderResultData.id,
						isOrder: true
					}
					
					return order;
				}

			}

		})


		// Create new Order Details

		if (orderIdPushed.isOrder !== false) {
			let isCourseUpdated = await OrderDetail.findOne({orderId: orderIdPushed.id, productId: data.productId}).then(resultData => {

				if (resultData === null) {

					let newOrderDetail = new OrderDetail({
						orderId: orderIdPushed.id,
						productId: data.productId,
						quantity: data.quantity,
						productName: isProductDetails.name,
						price: isProductDetails.price
					})

					return newOrderDetail.save().then((product, error) =>{
						if(error){
							return false
						}
						else{

							stockAvailable = (isProductDetails.stocks - (data.quantity))
							let updatedStocks = {
								stocks: stockAvailable
							}
							
							Product.findByIdAndUpdate(product.productId, updatedStocks).then(updatedStocks => updatedStocks)
							return `Add to cart successfully`
						}
					})
				}
				else {
					let quantity = 0;

					quantity = resultData.quantity + (data.quantity)
					if (quantity >= 0) {

						let updateOrderDetail = {
							quantity: quantity
						}

						// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
						return OrderDetail.findByIdAndUpdate(resultData.id, updateOrderDetail).then((orderDetailUpdates, error) =>{
							if(error){
								return false;
							}
							else{
								// return true;
								stockAvailable = (isProductDetails.stocks - (data.quantity))
								let updatedStocks = {
									stocks: stockAvailable
								}

								Product.findByIdAndUpdate(orderDetailUpdates.productId, updatedStocks).then(updatedStocks => updatedStocks)
								return `Added to cart successfully`;
							}
						})
					}
					else {
						// return false;
						return `Add to cart failed! Please check your quantity.`
					}

				}
			})
			return isCourseUpdated;

		}	

	}
	else
	{
		console.log(`false is this`)
		return `Sorry! Stocks available: ${isProductDetails.stocks}`;
	}

}

module.exports.viewCart = async(data) => {
	// console.log(`Data Result:  ${data.userId}`)
	let order = await Order.findOne({userId: data.userId, isCompleted: false}).then((orderResultData, error) => {
			// console.log(orderResultData)
			if(error){
				console.log(`System error: ${error}\n`);
				// return false;
				return `System error: ${error}`;
			}
			else {
				if (orderResultData !== null) {
					// return true;
					let orderCart = {
						id: orderResultData.id,
						userId: orderResultData.userId,
						isExcute: false 
					}
					return orderCart;	
				}
				else {
					return false;
				}
			}

	})

	let myOrder = await OrderDetail.find({orderId: order.id, isCompleted: false}).then(orderData => orderData.map(product => {
		// console.log(`Order Data:: ${product.productId}`)
	 	// return Product.findById(product.productId).then(resultData => resultData)

		let cart = {
			orderId: order.id,
			productId: product.productId,
			productName: product.productName,
			price: product.price,
			quantity: product.quantity
		}
		return cart

	}))
	
	return myOrder;

}




module.exports.checkout = async(data) => {
	console.log(`Data Result:  ${data.userId}`)
	let order = await Order.findOne({userId: data.userId, isCompleted: false}).then((orderResultData, error) => {
			console.log(orderResultData)
			if (orderResultData !== false) {
				if(error){
					console.log(`System error: ${error}\n`);
					// return false;
					return `System error: ${error}`;
				}
				else {
					if (orderResultData !== null) {
						// return true;
						let orderCart = {
							id: orderResultData.id,
							userId: orderResultData.userId,
							isExcute: true 
						}
						return orderCart;	
					}
					else {
						return false;
					}
				}
			}
			else {
				return false;
			}
			

	})
	if(order !== false){

		let sumAll = await OrderDetail.aggregate([{$match: {orderId: order.id}},{$group:{_id: "$orderId", sum: { $sum: {$multiply: ["$price", "$quantity"]} }}}]).then(sumData => {

			let x = {
				totalAmount: sumData[0].sum
			}
			return x;
		})

		if (order.isExcute !== false) {


			let updatedOrder = {
				contactNumber: data.contactNumber,
				shippingAddress: data.shippingAddress,
				modeOfPayment: data.modeOfPayment,
				totalAmount: sumAll.totalAmount,
				isCompleted: true
			}

			return Order.findByIdAndUpdate(order.id, updatedOrder).then((updated, error) =>{
				if(error){
					return false;
				}
				else{
					let isOrderComplete = {
						isCompleted: true
					}
					let checkoutOrder = {
						userId: data.userId,
						contactNumber: data.contactNumber,
						shippingAddress: data.shippingAddress,
						modeOfPayment: data.modeOfPayment,
						totalAmount: sumAll.totalAmount
					}
					console.log(updated)
					return checkoutOrder;
				}
			})

		}
		else {
			return false
		}

	}
	else {
		return `There are no items in the cart`
	}


}



// Controller for the all order
module.exports.getAllOrder = () => {
	return Order.find({isCompleted: true}).then(result => result)
}


// Controller for the all order
module.exports.getAllCompletedOrder = (data) => {
	return Order.find({id: data.userId, isCompleted: data.isCompleted}).then(result => result)
}