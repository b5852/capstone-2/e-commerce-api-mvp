// Require modules 
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderDetailRoutes = require("./routes/orderDetailRoutes");

// Create server
const app = express();

// port
const port = 4000;

// Connect to our MongoDB Database 
mongoose.connect("mongodb+srv://system:admin@myhelloword.t5o0pyw.mongodb.net/ecommerce-app?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

// Set Notification for connection success or failure
let db = mongoose.connection;

// if failed to connect the database
db.on("error", console.error.bind(console, "Connection Error"));

// if database connection successfully connected
db.once("open", () => console.log("We're connected to the cloud database"));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Routes for our API
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderDetailRoutes);

// listening to port
app.listen(process.env.PORT || port, () => {
	// 
	console.log(`Your capstone 2 project is now online on port ${process.env.PORT || port}`)
})




